(function(){
    
    var app = angular.module('controllers', ['jkuri.gallery']);
    
    app.controller('SectionManagerCtrl', [ '$scope', function($scope){

        $scope.isHomeVisible    = true;
        $scope.isContactVisible = false;
        $scope.isBarberVisible  = false;
        $scope.isStoreVisible   = false;
        $scope.isBarVisible     = false;
        $scope.isVideosVisible  = false;

        $scope.setBarberSection   = {};
        $scope.setStoreSection    = {};
        $scope.setBarSection      = {};
        $scope.setVideosSection   = {};
        $scope.setContactSection  = {};

        $scope.hideContent = function() {
            $scope.isHomeVisible    = false;
            $scope.isBarberVisible   = false;
            $scope.isStoreVisible   = false;
            $scope.isBarVisible     = false;
            $scope.isVideosVisible  = false;
            $scope.isContactVisible = false;
        };
        

        $scope.setGeneralSection = function() {
            $scope.hideContent();
            $scope.isHomeVisible = true;
        }
        
        $scope.setBarberSection = function() {
            $scope.hideContent();
            $scope.isBarberVisible = true;
        }
        
        $scope.setStoreSection = function() {
            $scope.hideContent();
            $scope.isStoreVisible = true;
        }
        
        $scope.setBarSection = function() {
            $scope.hideContent();
            $scope.isBarVisible = true;
        }
        
        $scope.setVideosSection = function() {
            $scope.hideContent();
            $scope.isVideosVisible = true;
        }
        
        $scope.setContactSection = function() {
            $scope.hideContent();
            $scope.isContactVisible = true;
        };
        
    }]);

    app.controller('GalleryCtrl', [ function($scope, $document) {
        
        var self = this;
        self.images = [
            
            {thumb: 'images/old-store/sample_zapas.jpg',   img: 'images/old-store/sample_zapas.jpg', description: 'Image 1'},
            {thumb: 'images/old-store/sample_zapas1.jpg',  img: 'images/old-store/sample_zapas1.jpg', description: 'Image 2'},
            {thumb: 'images/old-store/sample_zapas2.jpg',  img: 'images/old-store/sample_zapas2.jpg', description: 'Image 3'},
            {thumb: 'images/old-store/sample_gorro.jpg',   img: 'images/old-store/sample_gorro.jpg', description: 'Image 4'},
            {thumb: 'images/old-store/sample_jeans.jpg',   img: 'images/old-store/sample_jeans.jpg', description: 'Image 5'},
            {thumb: 'images/old-store/sample_remeras.jpg', img: 'images/old-store/sample_remeras.jpg', description: 'Image 6'}
        ];
            
    }]);

    app.controller('GalController', [ '$scope', function($scope) {
        
        var self = this;    
            
        self.isGalleryVisible = {};
            
        self.index = 0;    

        $scope.imagesRepo = [
            { src: 'images/old-store/sample_zapas.jpg',   alt: 'Imagen de prueba', text: 'imagen 1'},
            { src: 'images/old-store/sample_zapas1.jpg',  alt: 'Imagen de prueba', text: 'imagen 2'},
            { src: 'images/old-store/sample_zapas2.jpg',  alt: 'Imagen de prueba', text: 'imagen 3'},
            { src: 'images/old-store/sample_gorro.jpg',   alt: 'Imagen de prueba', text: 'imagen 4'},
            { src: 'images/old-store/sample_jeans.jpg',   alt: 'Imagen de prueba', text: 'imagen 5'},
            { src: 'images/old-store/sample_remeras.jpg', alt: 'Imagen de prueba', text: 'imagen 6'},

        ];

        self.showGallery = function() {
            isGalleryVisible = true;
        };
  
    }]);

})();