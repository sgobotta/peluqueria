(function(){
    
    var app = angular.module('main', ['controllers', 'jkuri.gallery']);
    
    app.directive('manoBanner', function(){
        return {
            restrict: 'E',
            templateUrl: 'html/banner.html'
        };
    });
    
    app.directive('peluLocalCarousel', function(){
        return{
            restrict: 'E',
            templateUrl: 'html/header/pelu-local-carousel.html'
        };
    });
    
    app.directive('aboutPeluqueria', function(){
        return{
            restrict: 'E',
            templateUrl: 'html/about/about-pelu.html'
        };
    });
    
    app.directive('aboutLocal', function(){
        return{
            restrict: 'E',
            templateUrl: 'html/about/about-local.html'
        };
    });
    
    app.directive('peloTeam', function(){
        return{
            restrict: 'E',
            templateUrl: 'html/team.html'
        };
    });
    
    app.directive('peloMaps', function(){
        return{
            restrict: 'E',
            templateUrl: 'html/pelu-maps.html'
        };
    });
    
    app.directive('peloContact', function(){
        return{
            restrict: 'E',
            templateUrl: 'html/contact.html'
        };
    });
    
    app.directive('peloFooter', function(){
        return{
            restrict: 'E',
            templateUrl: 'html/footer/footer-main.html'
        };
    });
    
    app.directive('barberGallery', function(){
        return{
            restrict: 'E',
            templateUrl: 'html/galleries/barber-gallery.html'
        };
    });
    
    app.directive('storeGallery', function(){
        return{
            restrict: 'E',
            templateUrl: 'html/galleries/old-store-gallery.html'
        };
    });    
    
//    app.directive('marcasSlide', function(){
//        return{
//            restrict: 'E',
//            templateUrl: 'html/marcas-slide.html'
//        };
//    });

    app.directive('enConstruccion', function(){
        return{
            restrict: 'E',
            templateUrl: 'html/en-construccion.html'
        };
    });
    
    app.directive('galleryTest', function(){
        return{
            restrict: 'E',
            templateUrl: 'html/gallery.html'
        };
    });

    
    
    app.directive('justified', ['$timeout', function ($timeout) {
        return {
            restrict: 'AE',
            link: function (scope, el, attrs) {
                scope.$watch('$last', function (n, o) {
                    if (n) {
                        $timeout(function () { $(el[0]).justifiedGallery(); });
                    }
                });
            }
        };
    }]);

    app.directive('repeatDone', [function () {
        return {
        restrict: 'A',
         link: function (scope, element, iAttrs) {
              var parentScope = element.parent().scope();
              if (scope.$last){
                   parentScope.$last = true;           
              }
            }
        };
    }]);


    app.controller('Ctrl', ['$scope', function($scope) {
            
        $scope.dirs = [{
                    id: 1,
                    name: "Dir 1",
                    first_image: "images/sample_zapas.jpg"
                }, {
                    id: 2,
                    name: "Dir 2",
                    first_image: "images/sample_zapas1.jpg"
                }, {
                    id: 3,
                    name: "Dir 3",
                    first_image: "images/sample_zapas.jpg2"
                }, {
                    id: 4,
                    name: "Dir 4",
                    first_image: "images/sample_zapas.jpg"
                }, {
                    id: 5,
                    name: "Dir 5",
                    first_image: "images/sample_zapas1.jpg"
                }, {
                    id: 6,
                    name: "Dir 6",
                    first_image: "images/sample_zapas2.jpg"
                }, {
                    id: 7,
                    name: "Dir 7",
                    first_image: "images/sample_zapas.jpg"
                }, {
                    id: 8,
                    name: "Dir 8",
                    first_image: "images/sample_zapas1.jpg"
                }, {
                    id: 9,
                    name: "Dir 9",
                    first_image: "images/sample_zapas2.jpg"
                }]; 
            
    }]);

})();